# README #

Chat application.

### What is this repository for? ###

This application is just online chat between users.

Version 1.0.

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

**ER_Diagram:** 

![ER_diagram.png](https://bitbucket.org/repo/kAper8/images/1887668317-ER_diagram.png)