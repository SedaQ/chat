<?php
// source: C:\xampp\htdocs\Chat\app\presenters/templates/CreateNewRoom/default.latte

class Template6498c4880b06b80d46d87ddc30fabe38 extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('a746c5a6a9', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block header
//
if (!function_exists($_b->blocks['header'][] = '_lba65b0fc1bd_header')) { function _lba65b0fc1bd_header($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
;
}}

//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lb91a66c86c7_content')) { function _lb91a66c86c7_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?><div id="wrapper">
    <div id="header">
        <a href="#">
            <div class="skype"></div>
        </a>
        <a href="#">
            <div class="linkedin"></div>
        </a>
        <a href="#">
            <div class="fb"></div>
        </a>
        <h1>Connecting people</h1>
    </div>
    <div id="content" align="center">
        <form id="login" action="charRoom.html" method ="POST">
            <h1> Create new room </h1>
            <fieldset id="inputs">
                <input id="username" type="text" placeholder="New room name" autofocus required><br>
                </br>
            </fieldset>
            <fieldset class="actions">
                <div class="goToRegistration">
                    <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Rooms:default"), ENT_COMPAT) ?>
">Back</a>
                    <input type="submit" class="submit" value="Create room">
                </div>
            </fieldset>
        </form>
    </div>
    <div id="footer">
            Projekt z WA na Mendelu vytvořil 
            <a href="https://is.mendelu.cz/auth/lide/clovek.pl?id=50753;"> Pavel Seda</a> © 2015
    </div>
</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIMacros::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['header']), $_b, get_defined_vars()) ; call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}