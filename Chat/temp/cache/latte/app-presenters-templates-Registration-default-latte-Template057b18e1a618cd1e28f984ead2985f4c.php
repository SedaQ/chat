<?php
// source: C:\xampp\htdocs\Chat\app\presenters/templates/Registration/default.latte

class Template057b18e1a618cd1e28f984ead2985f4c extends Latte\Template {
function render() {
foreach ($this->params as $__k => $__v) $$__k = $__v; unset($__k, $__v);
// prolog Latte\Macros\CoreMacros
list($_b, $_g, $_l) = $template->initialize('9444a444c0', 'html')
;
// prolog Latte\Macros\BlockMacros
//
// block content
//
if (!function_exists($_b->blocks['content'][] = '_lbd54e0dde2e_content')) { function _lbd54e0dde2e_content($_b, $_args) { foreach ($_args as $__k => $__v) $$__k = $__v
?><div id="wrapper">
    <div id="header">
        <a href="#">
            <div class="skype"></div>
        </a>
        <a href="#">
            <div class="linkedin"></div>
        </a>
        <a href="#">
            <div class="fb"></div>
        </a>
        <h1>Connecting people</h1>
    </div>
    <div id="content" align="center">
        <form id="register"<?php echo Nette\Bridges\FormsLatte\Runtime::renderFormBegin($form = $_form = $_control["registrationForm"], array (
  'id' => NULL,
), FALSE) ?>>
            <h1> Register </h1>
            <fieldset id="inputsRegister">
                <input id="usernameRegister" type="text" placeholder="Username" autofocus required<?php $_input = $_form["userName"]; echo $_input->{method_exists($_input, 'getControlPart')?'getControlPart':'getControl'}()->addAttributes(array (
  'id' => NULL,
  'type' => NULL,
  'placeholder' => NULL,
  'autofocus' => NULL,
  'required' => NULL,
))->attributes() ?>><br>
                <br>
                <input id="passwordRegister" type="password" placeholder="Password" required<?php $_input = $_form["password"]; echo $_input->{method_exists($_input, 'getControlPart')?'getControlPart':'getControl'}()->addAttributes(array (
  'id' => NULL,
  'type' => NULL,
  'placeholder' => NULL,
  'required' => NULL,
))->attributes() ?>><br>
                <br>
                <input id="email" type="text" placeholder="E-mail" required<?php $_input = $_form["email"]; echo $_input->{method_exists($_input, 'getControlPart')?'getControlPart':'getControl'}()->addAttributes(array (
  'id' => NULL,
  'type' => NULL,
  'placeholder' => NULL,
  'required' => NULL,
))->attributes() ?>><br>
                <br>
                <input id="name" type="text" placeholder="Name" required<?php $_input = $_form["name"]; echo $_input->{method_exists($_input, 'getControlPart')?'getControlPart':'getControl'}()->addAttributes(array (
  'id' => NULL,
  'type' => NULL,
  'placeholder' => NULL,
  'required' => NULL,
))->attributes() ?>><br>
                <br>
                <input id="surname" type="text" placeholder="Surname" required<?php $_input = $_form["surname"]; echo $_input->{method_exists($_input, 'getControlPart')?'getControlPart':'getControl'}()->addAttributes(array (
  'id' => NULL,
  'type' => NULL,
  'placeholder' => NULL,
  'required' => NULL,
))->attributes() ?>><br>
                <br>
            </fieldset>
<?php if ($form->ownErrors) { ?>            <ul class="error">
<?php $iterations = 0; foreach ($form->ownErrors as $error) { ?>                <li><?php echo Latte\Runtime\Filters::escapeHtml($error, ENT_NOQUOTES) ?></li>
<?php $iterations++; } ?>
            </ul>
<?php } ?>
            <fieldset id="actionsRegister">
                <div class="backToLogin">
                    <a href="<?php echo Latte\Runtime\Filters::escapeHtml($_control->link("Homepage:default"), ENT_COMPAT) ?>
">Back</a>
                    <input type="submit" class="submit"<?php $_input = $_form["send"]; echo $_input->{method_exists($_input, 'getControlPart')?'getControlPart':'getControl'}()->addAttributes(array (
  'type' => NULL,
  'class' => NULL,
))->attributes() ?>>
                </div>
            </fieldset>
        <?php echo Nette\Bridges\FormsLatte\Runtime::renderFormEnd($_form, FALSE) ?></form>
    </div>
    <div id="footer">
        Projekt z WA na Mendelu vytvořil 
        <a href="https://is.mendelu.cz/auth/lide/clovek.pl?id=50753;"> Pavel Seda</a> © 2015
    </div>
</div>
<?php
}}

//
// end of blocks
//

// template extending

$_l->extends = empty($_g->extended) && isset($_control) && $_control instanceof Nette\Application\UI\Presenter ? $_control->findLayoutTemplateFile() : NULL; $_g->extended = TRUE;

if ($_l->extends) { ob_start();}

// prolog Nette\Bridges\ApplicationLatte\UIMacros

// snippets support
if (empty($_l->extends) && !empty($_control->snippetMode)) {
	return Nette\Bridges\ApplicationLatte\UIMacros::renderSnippets($_control, $_b, get_defined_vars());
}

//
// main template
//
if ($_l->extends) { ob_end_clean(); return $template->renderChildTemplate($_l->extends, get_defined_vars()); }
call_user_func(reset($_b->blocks['content']), $_b, get_defined_vars()) ; 
}}