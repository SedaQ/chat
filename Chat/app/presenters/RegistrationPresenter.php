<?php

namespace App\Presenters;

use Nette,
    App\Model\UserManager, Nette\Application\UI\Form;

/**
 * Description of RegistrationPresenter
 *
 * @author Seda
 */

class RegistrationPresenter extends BasePresenter{
    
        private $database;
        private $users;
        private $uM;

                
	public function renderDefault()
	{
            $this->template->anyVariable = 'any value'; // odhlaseni pri neaktivite
	}
        
        public function __construct(Nette\Database\Context $database)
        {
            $this->uM = new UserManager($database);
            $this->database = $database;
        }
        
        /*
        protected function startup(){
            //parent::startup();
            //$this->users = $this->context->users;
        }
        */
        
        public function createComponentRegistrationForm() {
            $form = new Form;
            $form->addText('userName')
                    ->addRule(Form::FILLED, 'Vyplňte vaše user name')
                    ->addCondition(Form::FILLED);
            $form->addPassword('password', 'Heslo: *', 20)
                    ->setOption('description', 'Alespoň 6 znaků')
                    ->addRule(Form::FILLED, 'Vyplňte Vaše heslo')
                    ->addRule(Form::MIN_LENGTH, 'Heslo musí mít alespoň %d znaků.', 6);
            $form->addText('name')
                    ->addRule(Form::FILLED, 'Vyplňte vaše jméno')
                    ->addCondition(Form::FILLED);
            $form->addText('surname')
                    ->addRule(Form::FILLED, 'Vyplňte vaše přijmení')
                    ->addCondition(Form::FILLED);
            $form->addText('email', 'E-mail: *', 35)
                    ->setEmptyValue('@')
                    ->addRule(Form::FILLED, 'Vyplňte Váš email')
                    ->addCondition(Form::FILLED)
                    ->addRule(Form::EMAIL, 'Neplatná emailová adresa');
 
            $form->addSubmit('send', 'Registrovat');
            $form->onSuccess[] = callback($this, 'registerFormSubmitted');
            return $form;
        }    
        
        
        /*
        public function registration()
        {
            $user->setExpiration('30 minutes', TRUE);
            $user->setExpiration(0, TRUE); // odhlaseni pri neaktivite
        }    
        */
        
        public function registerFormSubmitted($form) {
            $values = $form->getValues();          
            try{
                $this->uM->add($values->userName, $values->password, $values->name, $values->surname, $values->email);
                //$this->flashMessage('Registrace se zdařila, jo!');
                $this->redirect('Rooms:default');
            }catch(Nette\Database\UniqueConstraintViolationException $ex){
                $form->addError($ex->getMessage());
            }
  
        }
        
}
