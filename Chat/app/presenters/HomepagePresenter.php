<?php

namespace App\Presenters;

use Nette,
	App\Model;


/**
 * Homepage presenter.
 */
class HomepagePresenter extends BasePresenter
{
        private $database;

        public function __construct(Nette\Database\Context $database)
        {
            $this->database = $database;
        }
    
    
	public function renderDefault()
	{
		$this->template->anyVariable = 'any value';
	}
        
        public function login()
        {
            try {
                // pokusíme se přihlásit uživatele...
                $user->login($username, $password);
                // ...a v případě úspěchu presměrujeme na další stránku
                //$this->redirect(...);

            } catch (Nette\Security\AuthenticationException $e) {
                echo 'Chyba: ', $e->getMessage();
            }   
        }

}
