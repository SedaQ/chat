<?php

namespace App\Presenters;

use Nette,
	App\Model;

/**
 * Description of ChatRoomPresenter
 *
 * @author Seda
 */
class ChatRoomPresenter extends BasePresenter{
    
        private $database;

        public function __construct(Nette\Database\Context $database)
        {
            $this->database = $database;
        }
        
	public function renderDefault()
	{
		$this->template->anyVariable = 'any value';
	}
}
